module "ansible-output" {
    source = "./modules/ansible-output"

    vms = [
        module.kube01,
        module.kube02,
        module.kube03,
    ]
}