terraform {
  required_providers {
    proxmox = {
      source = "bpg/proxmox"
      version = "0.43.3"
    }
  }
}

provider "proxmox" {
  alias = "h01"

  endpoint = "https://${var.proxmox_hosts.h01}:8006/"
  username = var.proxmox_username
  password = var.proxmox_passwords.h01
  insecure = true
  tmp_dir  = "/tmp"

  ssh {    
    agent = true
    node {
      name = "h01"
      address = var.proxmox_hosts.h01
    }
  }
}

provider "proxmox" {
  alias = "h02"

  endpoint = "https://${var.proxmox_hosts.h02}:8006/"
  username = var.proxmox_username
  password = var.proxmox_passwords.h02
  insecure = true
  tmp_dir  = "/tmp"

  ssh {    
    agent = true
    node {
      name = "h02"
      address = var.proxmox_hosts.h02
    }
  }
}

provider "proxmox" {
  alias = "h03"

  endpoint = "https://${var.proxmox_hosts.h03}:8006/"
  username = var.proxmox_username
  password = var.proxmox_passwords.h03
  insecure = true
  tmp_dir  = "/tmp"

  ssh {    
    agent = true
    node {
      name = "h03"
      address = var.proxmox_hosts.h03
    }
  }
}