variable "proxmox_username" {
    type = string
    description = "Username to use when accessing Proxmox"
}
variable "proxmox_passwords" {
    type = map(string)
    description = "Passwords to use when accessing Proxmox"
}
variable "proxmox_hosts" {
    type = map(string)
    description = "IP addresses to use when accessing Proxmox"
}