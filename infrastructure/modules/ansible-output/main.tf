resource "local_file" "host_vars" {
  for_each = zipmap(flatten([for item,value in var.vms : item]), flatten([for item, value in var.vms : value]))
  filename = "${path.root}/output/host_vars/${each.value.name}.yml"
  content  = <<EOF
mesh_ip: ${each.value.ipv4_address}
hosted_on: ${each.value.node}
EOF
}

resource "local_file" "pubkey" {
  for_each = zipmap(flatten([for item,value in var.vms : item]), flatten([for item, value in var.vms : value]))
  content  = each.value.ssh_public_key
  filename = "${path.root}/output/keys/${each.value.name}/id_rsa.pub"
}

resource "local_file" "privkey" {
  for_each = zipmap(flatten([for item,value in var.vms : item]), flatten([for item, value in var.vms : value]))
  content  = each.value.ssh_private_key
  file_permission = "0600"
  filename = "${path.root}/output/keys/${each.value.name}/id_rsa"
}
