variable "vms" {
    type = list(object({
        ipv4_address = string,
        ssh_private_key = string,
        ssh_public_key = string,
        root_password = string,
        node = string,
        name = string,
    }))
}