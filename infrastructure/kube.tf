module "kube01" {
  source = "git@gitlab.com:ztmesh/terraform-modules/vm.git?ref=v0.0.4"

  name = "kube01"
  node = "h01"
  providers = {
    proxmox = proxmox.h01
  }

  os = "debian12"
  cpu = 2
  memory = 3000
  disk_size = 20

  ipv4_address = "10.5.1.4/24"
  ipv4_gateway = "10.5.1.1"
}

module "kube02" {
  source = "git@gitlab.com:ztmesh/terraform-modules/vm.git?ref=v0.0.4"

  name = "kube02"
  node = "h02"
  providers = {
    proxmox = proxmox.h02
  }

  os = "debian12"
  cpu = 2
  memory = 3000
  disk_size = 20

  ipv4_address = "10.5.2.4/24"
  ipv4_gateway = "10.5.2.1"
}

module "kube03" {
  source = "git@gitlab.com:ztmesh/terraform-modules/vm.git?ref=v0.0.4"

  name = "kube03"
  node = "h03"
  providers = {
    proxmox = proxmox.h03
  }

  os = "debian12"
  cpu = 2
  memory = 3000
  disk_size = 20

  ipv4_address = "10.5.3.4/24"
  ipv4_gateway = "10.5.3.1"
}